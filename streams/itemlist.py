#! /usr/bin/env python
#
# Slice an ItemList out of an Athena logfile
#

import argparse
import ast
import os
import pprint
import sys

def main():
    parser = argparse.ArgumentParser(description="Slice an ItemList out of an Athena logfile for a named stream")
    parser.add_argument("LOGFILE", help="Athena logfile to process")
    parser.add_argument("STREAMNAME", help="Output stream to search for")
    parser.add_argument("--sort", action="store_true", help="Print stream items as a sorted list")
    parser.add_argument("--text", action="store_true", help="Print plain text with no pythonic list output")
    parser.add_argument("--format", action="store_true", help="Pretty print list as python, one line per item")

    args = parser.parse_args(sys.argv[1:])
    
    logfile = args.LOGFILE
    streamname = args.STREAMNAME
    logfiletrigger = "AugmentedPoolStream Stream{0}".format(streamname)

    # Use a simple state engine to get the correct piece of the logfile
    # ready -> steady -> go -> stop
    state = "ready"
    log = open(logfile)
    for line in log:
        if state=="ready":
            if logfiletrigger in line:
                state="steady"
        elif state=="steady":
            if "ItemList:" in line:
                state="go"
        elif state=="go":
            itemline = line.lstrip("0123456789: ") # Get rid of any timestamp prefix
            state = "stop"
        if state=="stop":
            break;

    if state != "stop":
        print >>sys.stderr, "Failed to find itemlist for stream {0} in {1} (reached state '{2}')".format(streamname, logfile, state)
        sys.exit(1)

    # literal_eval is safe!
    itemlist = ast.literal_eval(itemline)

    if args.sort:
        itemlist.sort()

    if args.text:
        for item in itemlist:
            print item
    else:
        if args.format:
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(itemlist)
        else:
            print itemlist


if __name__ == '__main__':
    main()
