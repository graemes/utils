#! /usr/bin/env python

import sys
import re


def main():
    with open(sys.argv[1]) as ifile:
        for line in ifile:
            line = line.strip()
            line = re.sub('  ', ' ', line)
            if line == "":
                print()
            else:
                print(line, end=" ")


if __name__ == '__main__':
    main()
