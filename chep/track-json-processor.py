#! /usr/bin/env python
#
# Preprocess abstract JSON data to produce track summaries and
# prepare a dataframe for a notebook

import json
import pandas as pd
import textwrap
import re
import argparse

rapporteur_map = {
    "T1": "steven.schramm@cern.ch",
    "T2": "teng.jian.khoo@cern.ch",
    "T3": "stefan.roiser@cern.ch",
    "T4": "alessandra.forti@cern.ch",
    "T5": "felice.pantaleo@cern.ch",
    "T6": "sofia.vallecorsa@cern.ch",
    "T7": "christoph.wissing@desy.de",
    "T8": "clara.nellist@cern.ch",
    "T9": "yangw@slac.stanford.edu"
}


def review_outcome(review):
    '''Defines the "pretty_print" string with the review recommendation'''
    result = review["proposed_action"]
    if result == "accept":
        if review["proposed_contrib_type"]:
            result += " as " + review["proposed_contrib_type"]["name"]
        else:
            result += " as UNKNOWN"
    elif result == "change_tracks":
        dtracks = [dtrack["code"] for dtrack in review["proposed_tracks"]]
        dtracks.sort()
        result += " to " + ";".join(dtracks)
    elif result == "merge":
        result += (" with abstract " +
                   str(review["proposed_related_abstract"]["friendly_id"]))
    return result


def add_review(result, friendly_id, target_track, track_summary):
    '''Adds the review result to the track and the global counter'''
    if result not in track_summary[target_track]:
        track_summary[target_track][result] = list()
    if result not in track_summary["Global"]:
        track_summary["Global"][result] = list()
    track_summary[target_track][result].append(friendly_id)
    track_summary["Global"][result].append(friendly_id)


def summary(track_summary, indexed_papers, paper_ids=True,
            judgement_crosscheck=True):
    paper_count = 0
    track_list = ["T" + str(track) for track in range(1, 10)]
    track_list.append("Global")
    for track in track_list:
        track_paper_count = 0
        print(f"Review Summary {track}")
        if track in track_summary:
            outcomes = list(track_summary[track].keys())
            outcomes.sort()
            for outcome in outcomes:
                print(" ", outcome + ":", len(track_summary[track][outcome]),
                      end="")
                if (paper_ids):
                    print(" ["
                          ", ".join([str(i) for i in
                                    track_summary[track][outcome]])
                          + "]")
                else:
                    print()
                track_paper_count += len(track_summary[track][outcome])
                if track != "Global":
                    paper_count += len(track_summary[track][outcome])
                    if judgement_crosscheck:
                        for paper in track_summary[track][outcome]:
                            x = do_judgement_crosscheck(outcome, paper,
                                                        indexed_papers)
                            if x:
                                print(f"Crosscheck Warning for {paper}: {x}")

        else:
            print("No summary available (nominate rapporteur)")
        print(f"Total: {track_paper_count}")
        print()
    print(f"Total Valid Track Papers: {paper_count}")


def do_judgement_crosscheck(review, paper_id, indexed_papers):
    '''Cross check the rapporteur review against the judgement.
    This returns the *problem*, so None is no problem.'''
    crosscheck = None
    if review.startswith("accept"):
        review_outcome = review.split()[2]
        if indexed_papers[paper_id]["state"] != "accepted":
            crosscheck = f"actual state is {indexed_papers[paper_id]['state']}"
        elif (indexed_papers[paper_id]["accepted_contrib_type"]["name"] 
              != review_outcome):
            if (review_outcome == "Plenary" and 
                indexed_papers[paper_id]["accepted_contrib_type"]["name"] == "Oral"):
                # This is ok
                crosscheck = None
                # crosscheck = ("(harmless) actual accept type is 
                #               f"{indexed_papers[paper_id]['accepted_contrib_type']['name']}") #NOQC
            else:
                crosscheck = ("actual accept type is "
                              f"{indexed_papers[paper_id]['accepted_contrib_type']['name']}")
    return crosscheck


def write_tricky_papers(track_summary, indexed_papers,
                        current="Special Paper Handling - tricky.csv", 
                        only_new=True):
    chairs = ("Caterina", "Doris", "Graeme", "Lucia")
    chair_counter = 0
    try:
        current = pd.read_csv(current)
        current_id_list = list(current["Paper ID"])
    except FileNotFoundError:
        # Not harmful - just set empty list of previous papers
        print("Warning - list of current trick papers not found")
        current_id_list = []
    printed_abstract = list()
    with open("tricky.csv", "w") as tp, open("special_abstracts_text.txt", "w") as ab:
        print('"Track", "Paper ID", "Title", "TC Recommendation", "PC Chair",'
              ' "PC Proposal"', file=tp)
        for track in ["T" + str(t) for t in range(1, 10)]:
            if track in track_summary:
                outcomes = list(track_summary[track].keys())
                outcomes.sort()
                for outcome in outcomes:
                    if (not outcome.startswith("accept") 
                        and outcome != "unreviewed"):
                        for paper_id in track_summary[track][outcome]:
                            if only_new and paper_id not in current_id_list:
                                print('"{track}", "{id}", "{title}", "{outcome}", "{chair}",'.format(track=track,
                                id=paper_id, title=indexed_papers[paper_id]["title"], outcome=outcome,
                                chair=chairs[chair_counter]),file=tp)
                                chair_counter += 1
                                if chair_counter == len(chairs):
                                    chair_counter = 0
                            else:
                                print(f"Paper {paper_id} is already known - skipping")
                            output_abstract(paper_id, indexed_papers, ab)
                            printed_abstract.append(paper_id)
                            if outcome.startswith("merge"):
                                m = re.search(r"(\d+)$", outcome)
                                if m:
                                    merge_id = int(m[1])
                                    print(merge_id)
                                    if merge_id not in printed_abstract:
                                        output_abstract(merge_id, indexed_papers, ab)
                                        printed_abstract.append(merge_id)


def write_global_summary(track_summary, indexed_papers):
    '''Write the final list of papers as a CSV'''
    with open("final_list.csv", "w") as final:
        print('"Track", "Outcome (Request)", "Id", "Title", "Abstract"', file=final)
        for track in [ "T" + str(t) for t in range(1,10) ]:
            for state, papers in track_summary[track].items():
                for paper in papers:
                    print(f'"{track}", "{state} ({indexed_papers[paper]["submitted_contrib_type"]["name"]})", "{paper}",',
                     f'"{indexed_papers[paper]["title"]}", ',
                     f'https://indico.cern.ch/event/773049/manage/abstracts/{indexed_papers[paper]["id"]}',
                     file=final)


def output_abstract(paper_id, indexed_papers, stream):
    print("Abstract {id}: {title} ({track})".format(id=paper_id,
    title=indexed_papers[paper_id]["title"],
    track=indexed_papers[paper_id]["reviewed_for_tracks"][0]["code"]),
    file=stream)
    print("Authors: {authors}\n".format(authors=author_str(indexed_papers[paper_id])), file=stream)
    print("\n".join(textwrap.wrap(indexed_papers[paper_id]["content"])), file=stream)
    print("\n", "---", "\n", sep="", file=stream)


def output_abstract_csv(paper_id, indexed_papers, stream):
    print('''"{id}", "{title}", "{track}", "{authors}", "{abstract}"'''.format(id=paper_id,
        title=indexed_papers[paper_id]["title"],
        track=indexed_papers[paper_id]["reviewed_for_tracks"][0]["code"],
        authors=author_str(indexed_papers[paper_id]),
        abstract=indexed_papers[paper_id]["content"].replace('"', '""')), file=stream)


def author_str(paper):
    author_list = list()
    for person in paper["persons"]:
        author_list.append("{first} {last}".format(first=person["first_name"],
        last=person["last_name"]))
    return "; ".join(author_list)


def write_promotions(track_summary, indexed_papers):
    count = 0
    with open("promotions.txt", "w") as promo, open("promotions.csv", "w") as pcsv:
        print('''"Id", "Title", "Track", "Authors", "Abstract"''', file=pcsv)
        for track in [ "T" + str(t) for t in range(1,10) ]:
            if "accept as Plenary" in track_summary[track]:
                for paper in track_summary[track]["accept as Plenary"]:
                    output_abstract(paper, indexed_papers, promo)
                    output_abstract_csv(paper, indexed_papers, pcsv)
                    count += 1
        print(f"Total {count} papers proposed", file=promo)


def write_cross_over_tracks(track_summary, indexed_papers):
    for track in [ "T" + str(t) for t in range(1,10) ]:
        for accept in ("oral", "poster"):
            with open(f"{track}-{accept}.csv", "w") as fout:
                print('''"Id", "Title", "Track", "Authors", "Abstract"''', file=fout)
                if accept == "oral":
                    papers = track_summary[track]["accept as Oral"]
                    if "accept as Plenary" in track_summary[track]:
                        papers += track_summary[track]["accept as Plenary"]
                else:
                    papers = track_summary[track]["accept as Poster"]
                papers.sort()
                for paper in papers:
                    output_abstract_csv(paper, indexed_papers, fout)




def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i',
        help="Input JSON file to process (default: %(default)s)",
        default="abstracts.json")
    parser.add_argument('--paperids', '-p', action='store_true',
        help="Print IDs of individual papers")
    parser.add_argument('--judgement-crosscheck', '-j', action='store_true',
        help="Cross check rapporteur review against judgement")
    options = parser.parse_args()

    with open(options.input) as abj:
        adata = json.load(abj)

    track_summary = dict()
    indexed_papers = dict()
    extra_states = ("withdrawn", "duplicate", "merged", "rejected")
    total_papers = 0

    # We have one per-track summary and one global one
    for track, rapporteur in rapporteur_map.items():
        track_summary[track] = dict()
    track_summary["Global"] = dict()

    for paper in adata["abstracts"]:
        # Make a map of friendly_id to papers for post processing
        indexed_papers[paper["friendly_id"]] = paper
        # Filter out entries that got merged or were withdrawn
        # and are thus not really counted per-track
        if paper["state"] in extra_states:
            if paper["state"] not in track_summary["Global"]:
                track_summary["Global"][paper["state"]] = list()
            track_summary["Global"][paper["state"]].append(paper["friendly_id"])
            total_papers += 1
            continue

        if len(paper["reviewed_for_tracks"]) != 1:
            print("WARNING - paper {0} is reviewed for tracks: ".format(paper["friendly_id"]), end="")
            for track in paper["reviewed_for_tracks"]:
                print(track["code"], " ", end="", sep="")
            print();

        target_track = paper["reviewed_for_tracks"][0]["code"]
        found_review = False
        # Find rapporteur review
        for review in paper["reviews"]:
            if review["user"]["email"] == rapporteur_map[target_track]:
                result = review_outcome(review)
                found_review = True
                break
        total_papers += 1
        if not found_review:
            result = "unreviewed"
        add_review(result, paper["friendly_id"], target_track, track_summary)

    summary(track_summary, indexed_papers, options.paperids, options.judgement_crosscheck)
    write_tricky_papers(track_summary, indexed_papers)
    write_global_summary(track_summary, indexed_papers)
    write_promotions(track_summary, indexed_papers)

    write_cross_over_tracks(track_summary, indexed_papers)

    print()
    print(f"Handled {total_papers} in total")

if __name__ == '__main__':
    main()
