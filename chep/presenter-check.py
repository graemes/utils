#! /usr/bin/env python
#
# Script to check for papers where there is a problem with
# the presenter, usually that they are missing or not yet
# registered

import argparse
import json
import pandas as pd

import smtplib
from email.message import EmailMessage

import os
import os.path
from datetime import datetime
import time


def is_registered(reg, name):
    '''Registration entry for name in reg dataframe'''
    pstat = reg[reg["Name"] == name]
    if len(pstat) == 1:
        return True
    else:
        return False


def reg_status(reg, name):
    '''Registration status for name in reg dataframe'''
    pstat = reg[reg["Name"] == name]
    if len(pstat) == 1:
        return pstat["Registration state"].values[0]
    else:
        return "No"


def get_abstract_emails(abstract):
    '''Find emails for submitters/authors of a given abstract'''
    emails = list()
    for person in abstract["persons"]:
        if len(person["email"]) > 0:
            emails.append(person["email"])
    if abstract["submitter"]["email"] not in emails:
        emails.append(abstract["submitter"]["email"])
    return emails


def author_report(f_id, contrib, emails):
    if len(emails) == 0:
        print("***WARNING*** no emails found: ", end='')
    if contrib["Presenters"].any():
        print("***WARNING*** found a contributer: ", end='')
    print(f_id, contrib[["Title", "Presenters"]][0:1].values, emails)


def set_message_and_subject(message_type, contrib):
    '''Set the message body and subject'''
    if message_type == "nomspeaker":
        body = f'''Dear CHEP Contributor(s)

We are writing to you in respect of your CHEP2019 contribution:

- {contrib["Title"].values[0]}
  ({contrib["Type"].values[0].lower()} in {contrib["Track"].values[0]})

This paper has been accepted for a presentation. However, as far as we can see
you have not yet nominated a speaker for the paper, depite an earlier email
from us. It is imperative that you do so *immediately*, so that we can cross
check that all contributions will be presented. Papers without a presenter will
have to be withdrawn.
'''
        if contrib["Type"].values[0] == "Oral":
            body += '''
Oral presentations which do not have a confirmed and registered speaker by
*4 October* will be at risk of being changed to poster presentations.

Note that no speaker is allowed to make more than 2 oral presentations in
the parallel track sessions.

'''

        body += '''
Please make sure that the presenter has registed for the conference:

https://indico.cern.ch/event/773049/registrations/

Due to some limitations in the Indico workflow it is not possible for you to
set the speaker, so please send an email to us as the PC Chairs and we will do
this for you. You can also send us any final updates to the author list.

Thank you again for your contribution and we look forward to seeing you
in Adelaide in November.

Kind regards

Caterina, Doris, Lucia, Graeme (PC Chairs)

P.S. If we did overlook a presenter nomination from you please accept our
apologies and thank you in advance for sending it again.
'''
        subject = ("CHEP2019, *Urgent* registered speaker nomination needed for "
                   f"{contrib['Title'].values[0]}")q

    elif message_type == "ctrbclarify":
        body = f'''Dear Contributor(s)

Further to our message today asking for a presenter nomination for

{contrib["Title"].values[0]}

we would like to clarify that "presentation" refers to both oral and
poster papers at CHEP. The type of your presentation has *not* changed.

In Indico even the presenters of posters are marked as "Speakers" and
we used that term, which caused confusion. Sorry about that.

All contributions to the conference must have a presenter, including
posters.

Kind regards

Caterina, Doris, Lucia, Graeme (PC Chairs)
'''
        subject = ("CHEP2019, clarification for paper "
                   f"{contrib['Title'].values[0]}")

    else:
        raise RuntimeError(f"Unknown message type: {message_type}")

    return subject, body


def send_email(f_id, contrib, emails, message_type, dry_run, logdir):
    subject, body = set_message_and_subject(message_type, contrib)

    msg = EmailMessage()
    msg.set_content(body)
    msg['Subject'] = subject
    msg['From'] = "graeme.andrew.stewart@cern.ch"
    msg['To'] = ",".join(emails)
    msg['Reply-to'] = "chep2019-pc-chairs@cern.ch"

    logf = os.path.join(logdir, f"{message_type}-email-{f_id}.txt")
    with open(logf, "w") as mailmsg_fp:
        print(msg, file=mailmsg_fp)

    if dry_run:
        print(f"Dry run option - email for paper {f_id} not sent")
        return

    print(f"Trying to send for paper {f_id}")
    s = smtplib.SMTP(host="cernmx.cern.ch", port=25)
    s.ehlo_or_helo_if_needed()
    # s.starttls()
    # s.ehlo()
    # s.login("graemes", os.environ["cpass"])
    s.send_message(msg)
    s.quit()
    print(f"Send for {f_id} seems to be done")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dry-run', '-n', action='store_true',
                        help=("Don't send any emails, just say "
                              "what would happen"))
    parser.add_argument('--ignore-ids', '-i',
                        help="File with a list of paper_ids to ignore",
                        default="ignore.txt")
    parser.add_argument('--whitelist', '-w', nargs='+',
                        type=int, default=list(),
                        help=("List of paper_ids for which email will be sent "
                              "(if specified all other papers are ignored)"))
    parser.add_argument('--force-email', '-f', nargs='+',
                        help=("List of email addresses that are forced for "
                              "any email sent (Indico found emails are "
                              "*ignored*)"))
    parser.add_argument('--pause', '-p', type=int, default=2,
                        help="Pause for PAUSE seconds between each email")
    parser.add_argument('--logdir', '-l',
                        default=("log-" +
                                 datetime.now().strftime("%Y-%m-%dT%H%M")),
                        help="Directory into which to log all actions")
    parser.add_argument('--type', '-t', choices=['Oral', 'Poster'],
                        help="Select only specific contributions")
    parser.add_argument('--message', '-m',
                        choices=['nomspeaker', 'ctrbclarify'],
                        help="Which message template to use",
                        required=True)
    options = parser.parse_args()

    ignore_ids = list()
    if options.ignore_ids:
        with open(options.ignore_ids) as ignore_ids_fp:
            for line in ignore_ids_fp:
                line = line.strip()
                if line.startswith('#') or line == "":
                    continue
                ignore_ids.append(int(line))

    if not os.path.isdir(options.logdir):
        os.mkdir(options.logdir)

    cntrb = pd.read_csv(f"contributions.csv")
    reg = pd.read_csv(f"registrations.csv")
    adata = json.load(open(f"abstracts.json"))

    indexed_papers = dict()
    for paper in adata["abstracts"]:
        # Make a map of friendly_id to papers for post processing
        indexed_papers[paper["friendly_id"]] = paper

    # Add some registration information to the contribution df
    cntrb["Registered"] = cntrb["Presenters"].apply(lambda x:
                                                    is_registered(reg, x))
    cntrb["Registration state"] = cntrb["Presenters"].apply(lambda x:
                                                            reg_status(reg, x))

    # Now loop over null presenter contributions
    id_log = os.path.join(options.logdir, "processed-ids.txt")
    with open(id_log, "w+") as processed_fp:
        print("# Processing run started", datetime.now().isoformat(),
              file=processed_fp)
        for f_id in cntrb[cntrb["Presenters"].isnull()]["Id"]:
            if (options.type and cntrb[cntrb["Id"] == f_id]["Type"].iloc[0] !=
                    options.type):
                print(f"Paper ID {f_id} is not {options.type}, skipping")
                continue

            emails = get_abstract_emails(indexed_papers[f_id])
            author_report(f_id, cntrb[cntrb["Id"] == f_id], emails)

            if f_id in ignore_ids:
                print(f"Paper ID {f_id} is in ignore list, skipping")
                continue
            if len(options.whitelist) > 0 and f_id not in options.whitelist:
                print(f"Paper ID {f_id} is not in whitelist, skipping")
                continue
            if options.force_email:
                print(f"Email addesses forced to {options.force_email}")
                emails = options.force_email

            send_email(f_id, cntrb[cntrb["Id"] == f_id], emails,
                       options.message, options.dry_run, options.logdir)

            if not options.dry_run:
                print(f_id, file=processed_fp)
            if options.pause > 0:
                time.sleep(options.pause)


if __name__ == "__main__":
    main()
