#! /usr/bin/env python
#
# Test sending emails at CERN...

import smtplib
from email.message import EmailMessage
import os


def main():
    with open("test-email.txt") as fp:
        # Create a text/plain message
        msg = EmailMessage()
        msg.set_content(fp.read())

    msg['Subject'] = "Test email"
    msg['From'] = "graeme.andrew.stewart@cern.ch"
    msg['To'] = "graeme.a.stewart@gmail.com, graemes.cern@gmail.com"
    msg['Reply-to'] = "chep2019-pc-chairs@cern.ch"

    s = smtplib.SMTP(host="smtp.cern.ch", port=587)
    s.ehlo_or_helo_if_needed()
    s.starttls()
    s.ehlo()
    s.login("graemes", os.environ["cpass"])
    s.send_message(msg)
    s.quit()


if __name__ == "__main__":
    main()
