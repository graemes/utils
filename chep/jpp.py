#! /usr/bin/env python

# Super simple JSON pretty printer, used when needing a human readable
# version of a "compact" JSON file

import json
import sys


def main():
    with open(sys.argv[1]) as inf:
        abs = json.load(inf)
        print(json.dumps(abs, sort_keys=True, indent=2))


if __name__ == '__main__':
    main()
