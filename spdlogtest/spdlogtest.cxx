#include <iostream>
#include <memory>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

void component_foo() {
  auto my_logger = spdlog::get("foo");

  my_logger->debug("It's foo time");
  my_logger->warn("Be warned");
  my_logger->critical("Fire in the potato bins");
}

void component_bar() {
  auto my_logger = spdlog::get("bar");

  my_logger->debug("It's bar time");
  my_logger->warn("Be again warned");
  my_logger->critical("Fire in the muffin bins");
}

int main() {

  try {
    // Sink that will be used by multiple loggers
    auto console = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

    // Two independent loggers
    auto foo_logger = std::make_shared<spdlog::logger>("foo", console);
    auto bar_logger = std::make_shared<spdlog::logger>("bar", console);
    // With independent levels
    foo_logger->set_level(spdlog::level::critical);
    bar_logger->set_level(spdlog::level::debug);
        
    // globally register the loggers so they can be accessed using spdlog::get(logger_name)
    spdlog::register_logger(foo_logger);
    spdlog::register_logger(bar_logger);
  }
  catch (const spdlog::spdlog_ex& ex) {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
  }

  component_foo();
  component_bar();

  return 0;
}

