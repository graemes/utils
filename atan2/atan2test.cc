#include <cmath>
#include <random>
#include <iostream>

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
 
double atan2fast(double y, double x)
{
    const double halfpi(M_PI / 2.);
    const double sixthpi(M_PI / 6.);
    const double tansixthpi(tan(M_PI / 6.));
    const double tantwelfthpi(tan(M_PI / 12.));
 
    bool complement(false);
    bool sign(false);
    bool region(false);
 
    // test zeros
    if (y == 0.0) return 0.0;
    if (x == 0.0) return halfpi;
 
    // normalize range to -pi/12 to pi/12
    double z = y / x;
    if (z < 0) {
        z = -z;
        sign = true;
    }
    if (z > 1.0) {
        z = 1.0 / z;
        complement = true;
    }
    if (z > tantwelfthpi) {
        z = (z - tansixthpi) / (1 + tansixthpi * z);
        region = true;
    }
 
    // compute approximate atan
    const double c1 = 1.6867629106;
    const double c2 = 0.4378497304;
    const double c3 = 1.6867633134;
 
    double zz = z * z;
    double v = (z * (c1 + zz * c2) / (c3 + zz));
 
    // go back to the original range
    if (region) v += sixthpi;
    if (complement) v = halfpi - v;
    if (sign) v = -v;
    if (x < 0.0) {
        if (v < 0.0) v += M_PI;
        else v -= M_PI;
    }
 
    return v;
}
 
int main() {
  double sum1 = 0;
  double sum2 = 0;
  std::mt19937 rng1;
  std::mt19937 rng2;
  rng1.seed(42);
  rng2.seed(22);
  std::uniform_real_distribution<> uint_dist1(-100,100);
  std::uniform_real_distribution<> uint_dist(-100,100);

  TFile outputFile("at2.root","RECREATE");
  TH1D histo("atan2", "atan2 to atan2fast comparison", 100, 0, 1.0e-4);
  TH2F badmap("badmap", "atan2 to atan2fast bad output (x,y where rel_error>1e-5)", 100, -100.0, 100.0, 100, -100.0, 100.0);
  
  for (int i = 0; i < 10000000; ++i) {
    double x = uint_dist(rng1);
    double y = uint_dist(rng2);
    sum1 += atan2fast(x, y);
    sum2 += std::atan2(x, y);
    double rel_diff =  std::abs((sum1-sum2)/sum1);
    histo.Fill(rel_diff);
    if (rel_diff > 1.0e-5) {
      std::cout << x << " " << y << std::endl;
      badmap.Fill(x, y);
    }
    //std::cout << (sum1-sum2)/sum1 << std::endl;
  }
  histo.Write();
  badmap.Write();
  std::cout << sum1 << "\t" << sum2 << std::endl;
}
