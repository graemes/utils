#! /usr/bin/env python

# Replaces multiple spaces with single spaces
# I don't really remember what I used this script for or why
# I did is this way instead of "s/\s+/ /"

import sys

with open(sys.argv[1]) as inf:
    for line in inf:
        line = line.replace("  ", " ").replace("  ", " ").strip()

        if line == "":
            print("")
        else:
            print(line, " ", end="")

print("")
