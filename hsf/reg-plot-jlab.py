#! /usr/bin/env python
#
# Cummulative plot of Indico registration data
#
# First version - too many things hardwired to Naples HSF workshop,
# but the key commands are all in...
import argparse
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU
import pandas as pd

def main():
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("inputfile", default="registrations.csv",
                            help="CSV file with Indico registration data")

        options = parser.parse_args()
    except Exception as e:
        parser.invalid_option_value(f'Parsing error: {e.msg}')
        return 1

    regData = pd.read_csv(options.inputfile)
    regData["mpd"] = pd.to_datetime(regData["Registration date"])
    range = regData["mpd"].max() - regData["mpd"].min()
    print(range, type(range))
    fig,ax = plt.subplots(1, 1)

    #locator = mdates.AutoDateLocator() - this really didn't seem to work well
    # and produced an uneven tick distribution
    weeks = mdates.WeekdayLocator(byweekday=MO, interval=2)
    dateFmt = mdates.DateFormatter('%Y-%m-%d')
    ax.xaxis.set_major_locator(weeks)
    ax.xaxis.set_major_formatter(dateFmt)

    ax.hist(regData["mpd"], bins=range.days+1, cumulative=True)

    # These lines represent early registration deadlines
    ax.axvline(datetime(2019, 2, 1), color='r', linestyle='dashed', linewidth=2)
    ax.axvline(datetime(2019, 2, 8), color='r', linewidth=2)

    ax.set_title("JLab Registrations")
    fig.autofmt_xdate()
    plt.show()

if __name__ == '__main__':
    main()
