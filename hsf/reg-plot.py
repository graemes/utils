#! /usr/bin/env python
#
# Cummulative plot of Indico registration data
#
# First version - too many things hardwired to Naples HSF workshop,
# but the key commands are all in...
import argparse
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd

def main():
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("inputfile", default="registrations.csv",
                            help="CSV file with Indico registration data")

        options = parser.parse_args()
    except Exception as e:
        parser.invalid_option_value(f'Parsing error: {e.msg}')
        return 1

    regData = pd.read_csv(options.inputfile)
    regData["mpd"] = pd.to_datetime(regData["Registration date"])
    fig,ax = plt.subplots(1, 1)

    ax.hist(regData["mpd"], bins=100, cumulative=True)

    # These lines represent early registration deadlines
    ax.axvline(datetime(2018, 2, 28), color='r', linestyle='dashed', linewidth=2)
    ax.axvline(datetime(2018, 3, 5), color='r', linewidth=2)

    ax.set_title("Naples Registrations")
    plt.show()

if __name__ == '__main__':
    main()
