#! /usr/bin/env python3
#
# Apply some standard tidy up to pandoc converted docx files, useful
# for converting Google Docs into HSF minutes
#

import argparse
import os
import re
import sys

relist = [
    [r'\[\[', '['],
    [r'\]\]', ']'],
    [r'- > ', '- '],
    [r'(\s+) > ', '\g<1> '],
    [r'^(\s+)$', ''], # This gets rid of a lot of excess space, but while
                      # need to put back a blank line before header elements
    ]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help="Input file to process")
    parser.add_argument('--backup', help="Backup filename for original file (default is INPUTNAME.bak)")
    options = parser.parse_args()

    if options.backup == None:
        options.backup = options.input + ".bak"

    os.rename(options.input, options.backup)

    with open(options.backup) as filein, open(options.input, 'w', newline='\n') as fileout:
        for line in filein:
            try:
                for regexpair in relist:
                    line = re.sub(regexpair[0], regexpair[1], line)
                if line.startswith('#'):
                    # Catch headers and make sure there is a preceeding blank line
                    # N.B. Use --atx-header option in pandoc conversion!
                    print(file=fileout)
                print(line, end='', file=fileout)
            except Exception as e:
                print("Exception raised processing '{0}': {1}", line, e)

if __name__ == '__main__':
    main()
