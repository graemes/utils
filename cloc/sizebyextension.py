#! /usr/bin/env python
#
# Count the size of files by extension (compliments CLOC, which counts
# lines of code)
#

# Make this code work in python3 or python2
from __future__ import print_function

import argparse
import os
import sys

from os.path import join, getsize

def sizeof_fmt(num, suffix='B'):
    '''Return a string for puny humans to understand
    From http://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size'''
    for unit in ['','k','M','G','T','P','E','Z']:
        if abs(num) < 1000.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1000.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def getExt(fname):
    filebits = fname.rsplit(".")
    if len(filebits) == 1:
        if fname == "ChangeLog":  # <- Care about this in particular!
            return "ChangeLog"
        else:
            return "noext"
    else:
        return filebits[-1]

def processDir(rootdirs):
    sizedict = {"total": {"size": 0.0, "n": 0}}
    
    for rootdir in rootdirs:
        if not os.path.isdir(rootdir):
            print ("{0} is not a directory!".format(rootdir), file=sys.stderr)
            return
    
        for root, dirs, files in os.walk(rootdir):
            for fname in files:
                if fname.endswith("~") or fname.startswith("."):
                    continue
                if ".git" in root:
                    continue
                ext = getExt(fname)
                try:
                    size = getsize(join(root, fname))
                    if ext in sizedict:
                        sizedict[ext]['size'] += size
                        sizedict[ext]['n'] += 1
                    else:
                        sizedict[ext] = {'size': size, 'n': 1}
                    sizedict["total"]["size"] += size
                    sizedict["total"]["n"] += 1
                except OSError:
                    pass

    return sizedict

def printstats(sizedict, args):
    exts = sizedict.keys()
    if args.sort:
        if args.sort in ['size', 'n']:
            exts.sort(key=lambda x: sizedict[x][args.sort], reverse=args.reverse)
        elif args.sort == 'avg':
            exts.sort(key=lambda x: sizedict[x]['size']/sizedict[x]['n'], reverse=args.reverse)

    if args.limit:
        if args.reverse:
            exts = exts[:args.limit]
        else:
            exts = exts[-args.limit:]

    for ext in exts:
        if args.human_readable:
            print(ext, sizedict[ext]['n'], sizeof_fmt(sizedict[ext]['size']), sizeof_fmt(sizedict[ext]['size']/sizedict[ext]['n']))
        else:
            print(ext, sizedict[ext]['n'], sizedict[ext]['size'], sizedict[ext]['size']/ sizedict[ext]['n'])

    return

def main():
    parser = argparse.ArgumentParser(description='Count sizes on disk by file extension')
    parser.add_argument('dirs', metavar='DIR', nargs='+', default='.',
                        help='directory roots to scan')
    parser.add_argument('--human-readable', action='store_true', default=False,
                        help='enable human readable numbers')
    parser.add_argument('--sort', default=False, choices=['size', 'n', 'avg', False],
                        help='sort output according to specified criterion')
    parser.add_argument('--reverse', default=False, action='store_true',
                        help='sort descending, instead of ascending')
    parser.add_argument('--limit', type=int, default=0,
                        help='only print limited entries')

    args = parser.parse_args()

    sizedict = processDir(args.dirs)
    printstats(sizedict, args)

    return 0


if __name__ == '__main__':
    main()
