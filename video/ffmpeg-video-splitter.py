#! /usr/bin/env python3

import argparse
import logging
import re
import subprocess
import sys

from pathlib import Path

logger = logging.getLogger(Path(sys.argv[0]).name)
logger.setLevel(logging.WARNING)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(name)s:%(levelname)s %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


def check_time(time_string: str) -> bool:
    """Sanity check that time string matches HH:MM:SS (a crude check for invalid strings)"""
    if not re.match(r"\d+(:\d+)*$", time_string):
        logger.error(
            f"Time string '{time_string}' does not seem to be valid HH:MM:SS format"
        )
        sys.exit(2)
    return True


def main():
    """Simple splitter for long video files, which will break them up into smaller
    files that can be posted on YouTube/CDS

    Usage: ffmpeg-video-splitter.py [-h] INPUT_VIDEO_FILE SPLITS_FILE

      INPUT_VIDEO_FILE - original long video file
      SPLITS_FILE - text file with lines giving start time, stop time and output filenames

      -h - print command help, listing all available options

    Example Splits File:

      # This is a comment
      00:30 15:20 Introduction
      16:25 59:30 First talk
      1:00:50 1:25:25 Second talk
      ...

    N.B. only two splits are done per-line, so whitespace in the filename is fine

    """

    ffmpeg_bin = (
        Path("c:/")
        / "Users"
        / "graem"
        / ".local"
        / "ffmpeg-5.0.1-full_build"
        / "bin"
        / "ffmpeg.exe"
    )
    output_dir = Path("output")

    parser = argparse.ArgumentParser(description="Split long video files using ffmpeg")
    parser.add_argument(
        "--index", type=int, help="Index counter value to prefix to output filename"
    )
    parser.add_argument("--prefix", help="Prefix to add to all output files")
    parser.add_argument("--dry-run", action="store_true")
    parser.add_argument(
        "--debug", action="store_true", help="Set logging level to debug"
    )
    parser.add_argument("--info", action="store_true", help="Set logging level to info")
    parser.add_argument(
        "--ffmpeg", help=f"Path to FFMPEG binary (default: {ffmpeg_bin})"
    )
    parser.add_argument(
        "--output", help=f"Directory for output split files (default: {output_dir})"
    )
    parser.add_argument("INPUT_VIDEO_FILE", help="Input video file to process")
    parser.add_argument(
        "SPLITS_FILE", help="File with timing and naming information for splits"
    )

    args = parser.parse_args(sys.argv[1:])
    if args.info:
        logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    if args.ffmpeg:
        ffmpeg_bin = Path(args.ffmpeg)
    if args.output:
        output_dir = Path(args.output)

    if not output_dir.is_dir():
        logger.debug(f"Creating output directory: {output_dir}")
        output_dir.mkdir(parents=True)

    if args.prefix:
        args.prefix += "_"
    else:
        args.prefix = ""

    try:
        with open(args.SPLITS_FILE, "r") as fp_splits:
            for line in fp_splits:
                cmd = [ffmpeg_bin, "-i", args.INPUT_VIDEO_FILE]
                try:
                    line = line.strip()
                    if line.startswith("#") or len(line) == 0:
                        continue
                    logger.debug(line)

                    start, stop, name = line.split(maxsplit=2)
                    check_time(start)
                    check_time(stop)

                    filename = args.prefix
                    if args.index != None:
                        filename += f"{args.index:02d}-"
                        args.index += 1
                    filename += name + ".mp4"
                    output_path = output_dir / filename
                    cmd.extend(["-ss", start, "-to", stop, output_path])

                except ValueError as e:
                    logger.error(f"Got a ValueError exception unpacking this line: {line}")
                    logger.error("Format should be: start_time stop_time output_file_name")
                    sys.exit(2)
                except Exception as e:
                    raise e

                logger.info(f"Executing: {cmd}")
                if not args.dry_run:
                    try:
                        subprocess.run(cmd)
                    except (subprocess.CalledProcessError, IOError) as e:
                        logger.error(f"Failure when executing ffmpeg subprocess: {e}")
                        logger.error(f"Command line: {cmd}")
                        sys.exit(1)
    except (FileNotFoundError, IOError) as e:
        logger.error(f"Failed to open splits file: {e}")

if __name__ == "__main__":
    main()
