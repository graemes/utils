#! /usr/bin/env python
#
# Mash the registration CSV into names that can be used for
# tutorial merge requests
#
with open("registrations.csv") as reg, open("names.txt", "w") as names:
    for line in reg:
        bits = line.split(",")
        name = bits[1].lower().replace(" ", "-")
        if name == "name":
            continue
        print >> names, name
