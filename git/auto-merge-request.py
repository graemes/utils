#! /usr/bin/env python
#
# Auto create a merge request for ATLAS git tutorial\
#
# Copyright (c) Graeme Andrew Stewart <graeme.a.stewart@gmail.com>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import os.path
import sys

from glogger import logger
from subprocess import check_call as run
from subprocess import check_output as grab


def prepare_branch(branch_name, target_branch):
    logger.info("Creating branch {0}".format(branch_name))
    run(["git", "checkout", "-b", branch_name, "upstream/{0}".format(target_branch), "--no-track"])

def apply_patch(patch_file, ampatch=False):
    logger.info("Applying patch {0}".format(patch_file))
    if ampatch:
        run(["git", "am", patch_file])
    else:
        run(["git", "apply", "--ignore-whitespace", patch_file])

def add_and_commit(commit_log):
    logger.info("Committing with log message from {0}".format(commit_log))
    run(["git", "add", "-A"])
    run(["git", "commit", "--all", "--file={0}".format(commit_log)])

def push_branch(branch_name, remote="origin"):
    logger.info("Pushing branch {0} to {1}".format(branch_name, remote))
    run(["git", "push", "--set-upstream", remote, branch_name])

def make_merge_request(branch_name, commit_log, target_projectid, source_projectid,
                       target_branch, ampatch, name):
    logger.info("Making merge request for {0}".format(branch_name))
    header_data = "PRIVATE-TOKEN: {0}".format(os.environ["PRIVATE_TOKEN"])
    if ampatch:
        commit_msg = grab(["git", "log", "-n1", "--pretty=format:%B"]).split("\n")
    else:
        with open(commit_log) as log_fh:
            commit_msg = log_fh.read().split("\n")
    commit_title = commit_msg[0]
    commit_message = "\n".join(commit_msg[2:])
    post_data = "target_project_id={target_projectid}&"\
                "target_branch={target_branch}&"\
                "source_branch={branch}&"\
                "title={title}&"\
                "description={msg}".format(target_projectid=target_projectid,
                                           target_branch=target_branch,
                                           branch=branch_name,
                                           title="{0} {1}".format(name, commit_title),
                                           msg=commit_message)
    url = "https://gitlab.cern.ch/api/v3/projects/{0}/merge_requests/".format(source_projectid)
    cmd = ["curl", "--header", header_data, "--data", post_data, url]
    print cmd
    run(cmd)

def go_patch(branch_name, target_branch, target_projectid, source_projectid, patch_file, commit_log,
             push=False, merge_req=False, ampatch=False, name=""):
    prepare_branch(branch_name, target_branch)
    apply_patch(patch_file, ampatch)
    if not ampatch:
        add_and_commit(commit_log)
    if push:
        push_branch(branch_name)
        if merge_req:
            make_merge_request(branch_name, commit_log, target_projectid, source_projectid,
                               target_branch, ampatch, name)


def main():
    parser = argparse.ArgumentParser(description='Automate merge request creation')
    parser.add_argument('--namelist', help="File with names of people to use as branch stubs")
    parser.add_argument('--testnames', nargs="+", help="List of names to use as a branch stubs")
    parser.add_argument('--patchlist', nargs="+", help="List of patches to use (round robin")
    parser.add_argument('--targetbranch', help="Target branch for merge requests (default %(default)s)",
                        default="21.0-tutorial")
    parser.add_argument('--targetprojectid', help="GitLab project ID for upstream repository (default %(default)s)",
                        default="17051")
    parser.add_argument('--sourceprojectid', help="GitLab project ID for origin repository (default %(default)s)",
                        default="17330")
    parser.add_argument('--patchpath', help="Path to directory with patch files (default %(default)s)",
                        default="../patches")
    parser.add_argument("--push", action="store_true", help="Push created branches to origin")
    parser.add_argument("--mergerequest", action="store_true", help="Create a merge request from pushed branches")
    parser.add_argument('--debug', '--verbose', "-v", action="store_true",
                        help="Switch logging into DEBUG mode")


    # Parse and handle initial arguments
    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)

    names = []
    if args.namelist:
        with open(args.namelist) as namefh:
            for name in namefh:
                name = name.strip()
                names.append(name)
    elif args.testnames:
        for name in args.testnames:
            names.append(name)
    else:
        logger.error("No names to process!")
        sys.exit(1)

    if not args.patchlist:
        logger.error("No patches to process!")
        sys.exit(1)

    patch_list_index = 0
    for name in names:
        branch_name = name + "-" + args.patchlist[patch_list_index]
        patch_file = os.path.join(args.patchpath, args.patchlist[patch_list_index] + ".patch")
        commit_log = os.path.join(args.patchpath, args.patchlist[patch_list_index] + ".msg")
        if not os.path.exists(commit_log):
            ampatch = True
            commit_log = None
        else:
            ampatch = False
        go_patch(branch_name=branch_name, target_branch=args.targetbranch,
                 target_projectid=args.targetprojectid, source_projectid=args.sourceprojectid,
                 patch_file=patch_file, commit_log=commit_log,
                 push=args.push, merge_req=args.mergerequest, ampatch=ampatch, name=name)
        patch_list_index += 1
        if patch_list_index == len(args.patchlist):
            patch_list_index = 0

    sys.exit(0)

if __name__ == '__main__':
    main()
