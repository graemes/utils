#! /usr/bin/env python
#
# Recursively ascend the directory tree until a .git directory is found
#

import os
import os.path

def git_repo_ok(path="."):
    entries = os.listdir(path)
    try:
        if (os.path.isfile(os.path.join(path, "HEAD")) and 
            os.path.isdir(os.path.join(path, "objects")) and 
            os.path.isdir(os.path.join(path, "refs"))):
            return True
    except OSError:
        return False
    return False


def main():
    # Iterate from cwd until we find a .git directory
    found_git = False
    here = os.getcwd()
    while True:
        if os.path.isdir(".git"):
            found_git = True
            break
        else:
            os.chdir("..")
            if os.getcwd() == here:
                break
            here = os.getcwd()

    if found_git and git_repo_ok(".git"):
        repo_root = os.path.abspath(here)
    else:
        repo_root = None

    if repo_root:
        print "Git repository root is here: {0}".format(repo_root)
        return 0
    
    print "You do not appear to be in a git repository"
    return 1

if __name__ == '__main__':
    main()
