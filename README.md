Graeme's Misc Toolbox
===

This repo just collects a few useful scripts and tools I have written.

Some are ATLAS specific, others just general tools.

Fork and develop at will!

Graeme <graeme.andrew.stewart@cern.ch>
