#! /bin/sh
#
# One shot script for building a full athena release in one go
# Can be used for testing the performance of different hardware
# setups
#

# Setup basic ATLAS environment to do the build in
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
lsetup git python
asetup none,gcc62 --cmakesetup

# Checkout athena repository and switch to known tag (uses krb5 URL)
(time git clone https://:@gitlab.cern.ch:8443/atlas/athena.git) |& tee output.clone
(time cd athena; git checkout release/21.0.23) |& tee output.switch-tag

# Now build athena's externals
(time ./athena/Projects/Athena/build_externals.sh) |& tee output.build-externals

# Now build athena itself, Release flavour (no debug symbols) and without the installation step
(time ./athena/Projects/Athena/build.sh -t Release -c -m -p) |& tee output.build-athena
