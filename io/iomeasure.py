#! /usr/bin/env python
#

import argparse
import os
import sys
import time

from subprocess import Popen, STDOUT, PIPE, CalledProcessError

## @brief Simple parser of /proc/PID/io, returning a python dictionary
#  @parameter pid PID to take i/o stats for
#  @return Dictionary of k:v pairs for io stats, empty dictionary if there was a problem
def readProcIO(pid):
    iostats = {}
    try:
        procStats = open(os.path.join("/proc", str(pid), "io"))
        for line in procStats:
            line = line.strip()
            (key, value) = line.split(": ")
            iostats[key] = value
    except IOError:
        # PID has gone?
        pass
    return iostats


## @brief List all processes and parents and form a dictionary where the 
#  parent key lists all child PIDs
#  @parameter listMyOrphans If this is @c True, then processes which share the same
#  @c pgid as this process and have parent PID=1 (i.e., init) get added to this process's children,
#  which allows these orphans to be added to the kill list. N.B. this means
#  that orphans have two entries - as child of init and a child of this
#  process
def getAncestry(listMyOrphans = False):
    psCmd = ['ps', 'ax', '-o', 'pid,ppid,pgid,args', '-m']

    try:
        p = Popen(psCmd, stdout=PIPE, stderr=PIPE)
        stdout = p.communicate()[0]
        psPID = p.pid
    except OSError, e:
        raise
    
    childDict = {}
    myPgid = os.getpgrp()
    myPid = os.getpid()
    for line in stdout.split('\n'):
        try:
            (pid, ppid, pgid, cmd) = line.split(None, 3)
            pid = int(pid)
            ppid = int(ppid)
            pgid = int(pgid)
            # Ignore the ps process
            if pid == psPID:
                continue
            if ppid in childDict:
                childDict[ppid].append(pid)
            else:
                childDict[ppid] = [pid]
            if listMyOrphans and ppid == 1 and pgid == myPgid:
                if myPid in childDict:
                    childDict[myPid].append(pid)
                else:
                    childDict[myPid] = [pid]
                
        except ValueError:
            # Not a nice line
            pass
    return childDict

## @brief Find all the children of a particular PID (calls itself recursively to descend into each leaf)
#  @note  The list of child PIDs is reversed, so the grandchildren are listed before the children, etc.
#  so signaling left to right is correct
#  @param psTree The process tree returned by @c trfUtils.listChildren(); if None then @c trfUtils.listChildren() is called internally.
#  @param parent The parent process for which to return all the child PIDs
#  @param listOrphans Parameter value to pass to getAncestry() if necessary
#  @return @c children List of child PIDs 
def listChildren(psTree = None, parent = os.getpid(), listOrphans = False):
    '''Take a psTree dictionary and list all children'''
    if psTree == None:
        psTree = getAncestry(listMyOrphans = listOrphans)
    
    children = []
    if parent in psTree:
        children.extend(psTree[parent])
        for child in psTree[parent]:
            children.extend(listChildren(psTree, child))
    children.reverse()
    return children


def main():
    parser = argparse.ArgumentParser(description='Watch io consumption of process and its children')
    parser.add_argument('pid', type=int, help="PID to watch")
    parser.add_argument('--verbose', action='store_true', help="Enable verbose outputs")
    parser.add_argument('--timestats', action='store_true', help="Print stats over time, as well as final values")
    parser.add_argument('--interval', type=float, default=5.0, help="Polling interval (default 5s)")

    args = vars(parser.parse_args())
    parentpid = args['pid']
    verbose = args['verbose']

    pidIOstats = {}
    isAlive = True
    while isAlive:
        children = listChildren(parent = parentpid)
        if verbose:
            print children
        for pid in children:
            iostats = readProcIO(pid)
            if iostats is not {}:
                pidIOstats[pid] = iostats
                if verbose:
                    print pid, iostats
        iostats = readProcIO(parentpid)
        if len(iostats) > 0:
            pidIOstats[parentpid] = iostats
            if verbose:
                print parentpid, iostats
            time.sleep(args['interval'])
        else:
            isAlive = False

    for pid, stats in pidIOstats.iteritems():
        for k, v in stats.iteritems():
            print str(pid)+":", k, "-", v

if __name__ == '__main__':
    main()
