#! /bin/sh
#
# This is a terrible script, but it mashes the output from iomeasure.py into a Jira table friendly format, so it's useful
#
perl -ne '@num=split(/[^\d]+/, $_); printf "| | %.3g | %.3g | %.3g | %.3g | %.3g | %.3g | %.3g\n", $num[2], $num[1], $num[3], $num[6], $num[5], $num[7], $num[4];' $1

