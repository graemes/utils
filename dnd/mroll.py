#! /usr/bin/env python
#
# Calculate the average of multiple dice rolls, where the higest
# dice are kept

import argparse

def roll_through(sides):
    for r in range(1, sides+1):
        yield r

def main():
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("--dice", default=20, type=int,
                            help="Sides on the dice to roll")
        parser.add_argument("--rolls", default=2, type=int,
                            help="Number of rolls to make")
        parser.add_argument("--keep", default=1, type=int,
                            help="Number of rolls to keep")
        parser.add_argument("--low", default=False, type=bool,
                            help="Keep lowest instead of highest")

        options = parser.parse_args()
    except Exception as e:
        parser.invalid_option_value(f'Parsing error: {e.msg}')
        return 1

    sum=0
    trials=0
    rolls=list()
    for d in range(options.rolls):
        rolls.append(0)
    finished=False
    # while(not finished):
    for r1 in range(1, options.dice+1):
        for r2 in range(1, options.dice+1):
            sum += max(r1,r2)
            trials += 1

    print(f"Performed {trials} trials, sum {sum}")
    print("Average:", sum/trials)


if __name__ == '__main__':
    main()
